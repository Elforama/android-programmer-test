package com.apppartner.androidprogrammertest.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.apppartner.androidprogrammertest.R;
import com.apppartner.androidprogrammertest.Tasks.FetchImageTask;
import com.apppartner.androidprogrammertest.models.ChatData;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Hashtable;
import java.util.List;

/**
 * Created on 12/23/14.
 *
 * @author Thomas Colligan
 *
 * Modified Lawrence Muller
 *
 * Applies a profile picture to chat taken from URL.
 * Caches images for fast loading
 * Saves images to disk for fast reloading and in case internet access is unavaliable 
 */
public class ChatsArrayAdapter extends ArrayAdapter<ChatData>{

    private static final String TAG = "ChatsArrayAdapter";

    private static final String FILENAME_PREFIX = "ProfilePic_";

    //Holds cached profile pictures to avoid reloading
    Hashtable<Integer, Bitmap> profilePics = new Hashtable<>();

    private Bitmap placeholderBitmap;

    public ChatsArrayAdapter(Context context, List<ChatData> objects){
        super(context, 0, objects);
        Drawable drawable = context.getResources().getDrawable(R.drawable.ic_launcher);
        placeholderBitmap = ((BitmapDrawable)drawable).getBitmap();
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        final ChatCell chatCell = new ChatCell();

        LayoutInflater inflater = LayoutInflater.from(getContext());
        convertView = inflater.inflate(R.layout.cell_chat, parent, false);

        chatCell.usernameTextView = (TextView) convertView.findViewById(R.id.usernameTextView);
        chatCell.messageTextView = (TextView) convertView.findViewById(R.id.messageTextView);
        chatCell.profileImageView = (ImageView) convertView.findViewById(R.id.profileImageView);

        final ChatData chatData = getItem(position);

        chatCell.usernameTextView.setText(chatData.username);
        chatCell.messageTextView.setText(chatData.message);

        //if we have the profile picture cached, use the cache
        if(profilePics.containsKey(chatData.userID)){

            Bitmap image = profilePics.get(chatData.userID);
            chatCell.profileImageView.setImageBitmap(image);

        //if we have the profile picture saved to disk
        //load it, and add it to the cache
        }else if(fileExistForID(chatData.userID)){

            Bitmap image = loadProfilePic(chatData.userID);
            profilePics.put(chatData.userID, image);
            chatCell.profileImageView.setImageBitmap(image);

        }else {//if there is no picture in the cache, fetch it from the web

            FetchImageTask fetchImageTask = new FetchImageTask(chatData.avatarURL);
            fetchImageTask.setResultsListener(new FetchImageTask.ResultsListener() {
                @Override
                public void onDownloadedImage(Bitmap image) {
                    chatCell.profileImageView.setImageBitmap(image);
                    profilePics.put(chatData.userID, image);
                    saveProfilePic(image, chatData.userID);

                    notifyDataSetInvalidated();
                }
            });
            fetchImageTask.execute();
            //Add the key with a placeholder bitmap while downloading the image
            profilePics.put(chatData.userID, placeholderBitmap);
        }
        return convertView;
    }

    private boolean saveProfilePic(Bitmap image, int id){
        try{
            FileOutputStream fos = getContext().openFileOutput(FILENAME_PREFIX + id, Context.MODE_PRIVATE);
            image.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
            return true;
        }catch (Exception e){
            Log.w(TAG, e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

    private boolean fileExistForID(int id){
        File filePath = getContext().getFileStreamPath(FILENAME_PREFIX + id);
        return filePath.exists();
    }

    private Bitmap loadProfilePic(int id){
        try {
            File filePath = getContext().getFileStreamPath(FILENAME_PREFIX + id);
            FileInputStream fi = new FileInputStream(filePath);

            Bitmap image = BitmapFactory.decodeStream(fi);

            fi.close();

            return image;
        } catch (Exception e) {
            Log.w(TAG, e.getMessage());
        }
        return null;
    }

    private static class ChatCell{
        ImageView profileImageView;
        TextView usernameTextView;
        TextView messageTextView;
    }
}
