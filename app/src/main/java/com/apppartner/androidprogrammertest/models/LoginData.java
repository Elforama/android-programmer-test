package com.apppartner.androidprogrammertest.models;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by jonathanmuller on 4/30/15.
 * Holds json result data when logging in
 */
public class LoginData {
    private static final String TAG = "LoginData";

    public String code;
    public String message;

    public LoginData(JSONObject jsonObject){
        if (jsonObject != null){
            try{
                code = jsonObject.getString("code");
                message = jsonObject.getString("message");
            }
            catch (JSONException e){
                Log.w(TAG, e);
            }
        }
    }
}
