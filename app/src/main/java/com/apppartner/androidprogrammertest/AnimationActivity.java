package com.apppartner.androidprogrammertest;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class AnimationActivity extends AppCompatActivity {

    private static final String TAG = "AnimationActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation);

        setupButton();
        setupIconDragging();

        setupToolBar();
    }

    private void setupToolBar(){
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("Animation");

        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    private void setupIconDragging() {
        final int width, height;

        width = getWindowManager().getDefaultDisplay().getWidth();
        height = getWindowManager().getDefaultDisplay().getHeight();

        final ImageView icon = (ImageView)findViewById(R.id.icon_imageView);
        final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)icon.getLayoutParams();

        //offset is used to place icons center on finger location
        final int xOffSet = params.width / 2;
        final int yOffSet = params.height / 2;

        icon.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_MOVE){

                    int x = (int)motionEvent.getRawX();
                    int y = (int)motionEvent.getRawY();

                    //set color values based on position
                    int red = x * 255 / width;
                    int green = y * 255 / height;
                    int blue = (red + green) / 2;
                    //apply color values
                    icon.setColorFilter(Color.argb(255, red, green, blue), PorterDuff.Mode.MULTIPLY);

                    //set icon position by modifying the padding
                    icon.setLeft(x - xOffSet);
                    icon.setTop(y - yOffSet);
                    icon.setBottom(y + xOffSet);
                    icon.setRight(x + yOffSet);
                }
                return true;
            }
        });
    }

    private void setupButton(){
        final ImageView icon = (ImageView)findViewById(R.id.icon_imageView);
        ImageView fadeBtnImageView = (ImageView)findViewById(R.id.fade_imageView);

        final Animation animationFadeIn = AnimationUtils.loadAnimation(this, R.anim.abc_fade_in);
        final Animation animationFadeOut = AnimationUtils.loadAnimation(this, R.anim.abc_fade_out);
        final Animation animationSpin = AnimationUtils.loadAnimation(this, R.anim.spin_shrink);

        animationFadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation) {
                //when icon finishes fading, fade it back in
                icon.startAnimation(animationFadeIn);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });

        fadeBtnImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                icon.startAnimation(animationFadeOut);
            }
        });

        fadeBtnImageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                icon.startAnimation(animationSpin);
                return true;
            }
        });
    }
}
