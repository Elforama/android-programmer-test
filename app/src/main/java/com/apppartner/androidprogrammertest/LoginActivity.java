package com.apppartner.androidprogrammertest;

import android.support.v7.app.ActionBar;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.apppartner.androidprogrammertest.Tasks.UserLoginTask;
import com.apppartner.androidprogrammertest.models.LoginData;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity{

    private static final String TAG = "LoginActivity";

    private UserLoginTask mLoginTask;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ImageView loginButton = (ImageView)findViewById(R.id.login_imageView);
        final EditText userNameText = (EditText)findViewById(R.id.username_editText);
        final EditText passwordText = (EditText)findViewById(R.id.password_editText);

        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/Jelloween - Machinato.ttf");
        userNameText.setTypeface(tf);
        passwordText.setTypeface(tf);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login(userNameText.getText().toString(), passwordText.getText().toString());
            }
        });

        setupToolBar();

    }

    /**
     * Sets title of toolbar and applies it as actionbar
     */
    private void setupToolBar(){
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("Login");

        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    /**
     * Attempts to log user in with a name and password
     * @param username
     * @param password
     */
    private void login(String username, String password){
        if(mLoginTask != null) {
            mLoginTask.cancel(true);
            mLoginTask = null;
        }
        mLoginTask = new UserLoginTask();
        mLoginTask.setURL(getResources().getString(R.string.login_request));
        mLoginTask.setResultsListener(new UserLoginTask.ResultsListener(){
            @Override
            public void onResults(String data){
                Log.d(TAG, data);
                LoginData loginData  = parseJsonData(data);
                showAlertDialog(loginData);
            }
        });

        //set the username and password the user entered
        mLoginTask.setUserName(username);
        mLoginTask.setPassword(password);
        //login the user
        mLoginTask.execute();
    }

    /**
     * Creates alert dialog show status of login attempt
     * @param data
     */
    private void showAlertDialog(final LoginData data){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(data.code);
        builder.setMessage(data.message + "\n(" + mLoginTask.getApiCallTime() + ")");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i){
                if(data.code.equals("Success"))
                    finish();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();

        //Center the dialog in the alert
        TextView message = (TextView)dialog.findViewById(android.R.id.message);
        message.setGravity(Gravity.CENTER);

        dialog.show();
    }

    /**
     * Parses the json data and returns it as LoginData
     * @param jsonString
     * @return
     */
    private LoginData parseJsonData(String jsonString){
        JSONObject jsonData = null;
        try {
            jsonData = new JSONObject(jsonString);
        } catch (JSONException e) {
            Log.w(TAG, e.getMessage());
            e.printStackTrace();
        }
        LoginData loginData = new LoginData(jsonData);
        return loginData;
    }




}
