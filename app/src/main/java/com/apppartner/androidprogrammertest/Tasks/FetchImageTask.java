package com.apppartner.androidprogrammertest.Tasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;

/**
 * Created by jonathanmuller on 4/28/15.
 */
public class FetchImageTask extends AsyncTask<Void, Void, Bitmap>{
    private static final String TAG = "UserLoginTask";

    private String mURL;

    private ResultsListener resultsListener;

    public FetchImageTask(String url){
        mURL = url;
    }

    public FetchImageTask(){

    }

    public void setURL(String url){
        mURL = url;
    }

    public void setResultsListener(ResultsListener listener){
        resultsListener = listener;
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(mURL);

        try{
            HttpResponse response = client.execute(httpGet);

            final int statusCode = response.getStatusLine().getStatusCode();

            if(statusCode != HttpStatus.SC_OK){
                Log.w(TAG, "Error " + statusCode + " while retrieving bitmap from " + mURL);
                return null;
            }

            HttpEntity entity = response.getEntity();
            if(entity != null){
                InputStream inputStream = null;
                try{
                    inputStream = entity.getContent();

                    return BitmapFactory.decodeStream(inputStream);
                }finally {
                    if(inputStream != null)
                        inputStream.close();
                    entity.consumeContent();
                }
            }

        }catch (Exception e){
            httpGet.abort();
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(final Bitmap image) {
        if(resultsListener != null)
            resultsListener.onDownloadedImage(image);
    }

    public static abstract class ResultsListener{
        public abstract void onDownloadedImage(Bitmap image);
    }
}
