package com.apppartner.androidprogrammertest.Tasks;

/**
 * Created by jonathanmuller on 4/27/15.
 */

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;


public class UserLoginTask extends AsyncTask<String, Void, String> {

    private static final String TAG = "UserLoginTask";

    private String mUserName;
    private String mPassword;
    private String mURL;

    private long apiCallTime = 0;

    private ResultsListener resultsListener;

    public UserLoginTask(String userName, String password, String url){
        mUserName = userName;
        mPassword = password;
        mURL = url;
    }

    public UserLoginTask(){

    }

    public void setUserName(String userName){
        mUserName = userName;
    }

    public void setPassword(String password){
        mPassword = password;
    }

    public void setURL(String url){
        mURL = url;
    }

    public void setResultsListener(ResultsListener listener){
        resultsListener = listener;
    }

    public long getApiCallTime(){
        return apiCallTime;
    }

    @Override
    protected String doInBackground(String... params) {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(mURL);

        //Set the username and password
        List<NameValuePair> list = new ArrayList<>(2);
        list.add(new BasicNameValuePair("username", mUserName));
        list.add(new BasicNameValuePair("password", mPassword));

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(list, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
        }

        StringBuilder stringBuilder = new StringBuilder();

        try{
            long startTime = System.currentTimeMillis();

            HttpResponse httpResponse = client.execute(httpPost);

            apiCallTime = System.currentTimeMillis() - startTime;

            InputStream inputStream = httpResponse.getEntity().getContent();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String tmp = "";

            while((tmp = bufferedReader.readLine()) != null){
                stringBuilder.append(tmp);
            }

        }catch (Exception e){
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
        }

        return stringBuilder.toString();
    }

    @Override
    protected void onPostExecute(final String data) {
        if(resultsListener != null)
            resultsListener.onResults(data);
    }

    public static abstract class ResultsListener{
        public abstract void onResults(String data);
    }
}
