package com.apppartner.androidprogrammertest.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

import com.apppartner.androidprogrammertest.R;

/**
 * Created by jonathanmuller on 4/29/15.
 */
public class CustomFontTextView extends TextView {

    private static final String TAG = "CustomFontTextView";

    private final static int MechinatoExtraLight = 0;
    private final static int MechinatoSemiBoldItalic = 1;
    private final static int MechinatoBold = 2;
    private final static int MechinatoRegular = 3;
    private final static int MechinatoLight = 4;

    public CustomFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        parseAttributes(context, attrs);
    }

    public CustomFontTextView(Context context, AttributeSet attrs, int defStyle){
        super(context, attrs, defStyle);
        parseAttributes(context, attrs);
    }

    private void parseAttributes(Context context, AttributeSet attrs){
        TypedArray values = context.obtainStyledAttributes(attrs, R.styleable.CustomFontTextView);

        int typeface = values.getInt(R.styleable.CustomFontTextView_typeface, 0);

        Typeface tf;

        switch (typeface){
            case MechinatoExtraLight:
                tf = Typeface.createFromAsset(context.getAssets(), "fonts/Jelloween - Machinato ExtraLight.ttf");
                break;
            case MechinatoSemiBoldItalic:
                tf = Typeface.createFromAsset(context.getAssets(), "fonts/Jelloween - Machinato Bold Italic.ttf");
                break;
            case MechinatoBold:
                tf = Typeface.createFromAsset(context.getAssets(), "fonts/Jelloween - Machinato Bold.ttf");
                break;
            case MechinatoRegular:
                tf = Typeface.createFromAsset(context.getAssets(), "fonts/Jelloween - Machinato.ttf");
                break;
            case MechinatoLight:
                tf = Typeface.createFromAsset(context.getAssets(), "fonts/Jelloween - Machinato Light.ttf");
                break;
            default:
                tf = Typeface.createFromAsset(context.getAssets(), "fonts/Jelloween - Machinato.ttf");
                break;
        }

        this.setTypeface(tf);

        values.recycle();
    }
}
